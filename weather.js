"use strict"

var request = require('request')

//var weather = ['day', 'temp', 'temp_min', 'temp_max', 'humidity', 'description'];

const url = 'http://api.openweathermap.org/data/2.5/forecast/city?id=524901&APPID=bfe9e8f492e6559fe2ad35be2d32f2a1'

exports.search = function(searchTerm, callback) {
    var weather = []
    console.log("Searching for: ", searchTerm);
    const query_string = {q: searchTerm}
    request.get({url: url, qs: query_string}, function(err, res, body) {
    if (err) {
      console.log(err)
    } else {
      /* convert the JSON-formatted string into a JavaScript object 'json' */
      const json = JSON.parse(body)
      const list = json.list
      console.log(list[0])
      list.forEach(function(val) {
        console.log(val.dt_txt)
        let item = {date: val.dt_txt, temp: val.main.temp, temp_min: val.main.temp_min, temp_max: val.main.temp_max, humidity: val.main.humidity}
        weather.push(item)
      })
      /* convert a JavaScript object 'json' into a nicely formatted string for printing */
      //const pretty = JSON.stringify(json, null, 2)
      /* print the http response status code */
      console.log('https://books-slgarrett1.c9users.io:8080/weather'+res.statusCode)
      //console.log(pretty)
      callback(weather)
    }
  })
}