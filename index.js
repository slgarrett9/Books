
var restify = require('restify')
var server = restify.createServer()

/* import the required plugins to parse the body and auth header. */
server.use(restify.fullResponse())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())

const weather = require('./weather')

server.get('/weather', function (req, res) {
    // this is where you access the DB OR access an external API for data
    var data = weather;
    var dataJson = JSON.stringify(data);
    res.setHeader('content-type', 'application/json');
    res.send(200, dataJson);
    res.end();
});

server.get('weather/:location', function(req, res) {
    const loc = req.params.location
    weather.search(loc, function(data) {
      res.setHeader('content-type', 'application/json')
      res.send(200, data)
      res.end()
    })
})

server.post('/weather', function (req, res) {
    const body = req.body;
    weather.push(body);
    console.log(body);
    res.send(201, "'Success!");
    res.end();
})

var port = process.env.PORT || 8080;
server.listen(port, function (err) {
  if (err) {
      console.error(err);
  } else {
    console.log('App is ready at : ' + port);
  }
})