var request = require('request')

const url = 'http://api.openweathermap.org/data/2.5/forecast?lat=52.2500000&lon=-0.8833300&appid=bfe9e8f492e6559fe2ad35be2d32f2a1'

exports.search = function(searchTerm) {
    const query_string = {q: searchTerm}
    request.get({url: url, qs: query_string}, function(err, res, body) {
    if (err) {
      console.log(err)
    } else {
      console.log(body)
      /* convert the JSON-formatted string into a JavaScript object 'json' */
      const json = JSON.parse(body)
      /* convert a JavaScript object 'json' into a nicely formatted string for printing */
      const pretty = JSON.stringify(json, null, 2)
      /* print the http response status code */
      console.log('STATUS CODE: '+res.statusCode)
      console.log(pretty)
    }
  })
}